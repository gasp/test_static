#!/usr/bin/env bash

docker run -it --volume=$(pwd):/usr/src/app --workdir="/usr/src/app" --entrypoint=/bin/bash node:8.4.0
