var assert = require('assert'),
    path = require('path'),
    fs = require('fs'),
    vows = require('vows'),
    request = require('request'),
    httpServer = require('http-server');

var root = path.join(__dirname, 'fixtures');

vows.describe('http-server').addBatch({
  'When http-server is listening on 8080': {
    topic: function () {
      var server = httpServer.createServer({
        root: root,
        robots: true,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        }
      });

      server.listen(8080);
      this.callback(null, server);
    },
    'it should serve files from root directory': {
      topic: function () {
        request('http://127.0.0.1:8080/sample.txt', this.callback);
      },
      'status code should be 200': function (res) {
        assert.equal(res.statusCode, 200);
      }
    },
    'it should send 404 on missing file': {
      topic: function () {
        request('http://127.0.0.1:8080/doesnotexist.txt', this.callback);
      },
      'status code should be 404': function (res) {
        assert.equal(res.statusCode, 404);
      }
    }
  }
}).export(module);
