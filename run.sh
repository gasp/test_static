APP_NAME=test_static
VIRTUAL_HOST=static.test.digitas.xyz
LETSENCRYPT_EMAIL=postmaster@digitas.xyz

echo "docker: deleting containers"
docker rm -f $APP_NAME

echo "docker: creating containers";
docker-compose -f docker-compose.yml up -d
