const httpServer = require("http-server");
const port = 8080;
const server = httpServer.createServer({
  root: "./html",
});
server.listen(port, "0.0.0.0");

console.log(`serving static server on ${port} in ${process.env.NODE_ENV}`);
